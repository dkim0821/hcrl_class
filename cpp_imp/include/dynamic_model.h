#ifndef DYN_MODEL
#define DYN_MODEL
#include <iostream>
#include <Eigen/Dense>
#include <math.h>
#include <nlopt.h>
#include <stdio.h>

using namespace Eigen;

class DynModel{
public:
	int state_dim;
	int control_dim;
	MatrixXf A;
	MatrixXf B;
	VectorXf x;
	VectorXf u;	

	double dt;

	void set_xo_uo(VectorXf &xo_in, VectorXf &uo_in);
	VectorXf xdot_state_transition(VectorXf &xo, VectorXf &uo);
	void print_matrix(MatrixXf &mat);
	void print_vector(VectorXf &vec);	
	void print_state();

	DynModel(int state_dimensions, int control_dimensions);
	~DynModel();

};
#endif
