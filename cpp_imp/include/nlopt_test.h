#ifndef NLOPT_TEST
#define NLOPT_TEST

#include <nlopt.h>
#include <math.h>
#include <iostream>
#include <stdio.h>

typedef struct {
    double a, b;
} my_constraint_data;

class nlopt_test{
public:
    nlopt_test();
    ~nlopt_test();

    my_constraint_data data1;
    my_constraint_data data2;
    
    
    nlopt_opt opt_;
    void DoOptimization();
    static double myconstraint(unsigned n, const double *x, double *grad, void *data);

    static double myfunc(unsigned n, const double *x, double *grad, void *my_func_data);

};

#endif
