#include "nlopt_test.h"


nlopt_test::nlopt_test(){
    double lb[2] = { -HUGE_VAL, 0 }; // lower bounds 
    opt_ = nlopt_create(NLOPT_LD_MMA, 2); // algorithm and dimensionality
    nlopt_set_lower_bounds(opt_, lb);
    nlopt_set_min_objective(opt_, nlopt_test::myfunc, NULL);


    data1 = {2,0};
    data2 = {-1, 1};
    // my_constraint_data data[2] = { {2,0}, {-1,1} };
    
    nlopt_add_inequality_constraint(opt_, nlopt_test::myconstraint, &data1, 1e-8);
    nlopt_add_inequality_constraint(opt_, nlopt_test::myconstraint, &data2, 1e-8);

    nlopt_set_xtol_rel(opt_, 1e-4);

}

nlopt_test::~nlopt_test(){
    nlopt_destroy(opt_);
}


void nlopt_test::DoOptimization(){
    double x[2] = { 1.234, 5.678 };  // some initial guess 
    double minf; // the minimum objective value, upon return 
    
    if (nlopt_optimize(opt_, x, &minf) < 0) {
        printf("nlopt failed!\n");
    }
    else {
        printf("found minimum at f(%g,%g) = %0.10g\n", x[0], x[1], minf);
    }
}

double nlopt_test::myconstraint(unsigned n, const double *x, double *grad, void *data) {
    my_constraint_data *d = (my_constraint_data *) data;
    double a = d->a, b = d->b;
    if (grad) {
        grad[0] = 3 * a * (a*x[0] + b) * (a*x[0] + b);
        grad[1] = -1.0;
    }
    return ((a*x[0] + b) * (a*x[0] + b) * (a*x[0] + b) - x[1]);
 }


double nlopt_test::myfunc(unsigned n, const double *x, double *grad, void *my_func_data) {
    if (grad) {
        grad[0] = 0.0;
        grad[1] = 0.5 / sqrt(x[1]);
    }
    return sqrt(x[1]);
}

int main(int argc, char const *argv[]){
    nlopt_test test_obj;
    test_obj.DoOptimization();
}