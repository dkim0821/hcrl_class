#include "dynamic_model.h"

DynModel::DynModel(int state_dimensions, int control_dimensions): 
										state_dim(state_dimensions), 
										control_dim(control_dimensions) {
	A.resize(state_dim, state_dim);
	B.resize(state_dim, control_dim);
	x.resize(state_dim);
	u.resize(control_dim);	
	dt = 0.001;
}
DynModel::~DynModel(){}


void DynModel::set_xo_uo(VectorXf &xo_in, VectorXf &uo_in){
	x = xo_in;
	u = uo_in;	
}

VectorXf DynModel::xdot_state_transition(VectorXf &xo, VectorXf &uo){
	MatrixXf I_x = MatrixXf::Identity(state_dim, state_dim);
	//return (A + I_x*dt)*xo + B*dt*uo;	
	return (A*xo + B*uo);
}

void DynModel::print_matrix(MatrixXf &mat){
	for(size_t i = 0; i < mat.rows(); i++){
		for(size_t j = 0; j < mat.cols(); j++){
			std::cout << mat(i,j) << ' ' ;
		}
		std::cout << std::endl;
	}
}

void DynModel::print_vector(VectorXf &vec){
	for(size_t i = 0; i < vec.rows(); i++){
		std::cout << vec(i) << ' ' ;
	}
}

void DynModel::print_state(){
	std::cout << "[ ";
	for(size_t i = 0; i < x.rows(); i++){
		std::cout << x(i) << " " ;
	}
	std::cout << "]" << std::endl;
}

// Excercise Model from MPC paper.
void assign_Matrix_coeffs(MatrixXf &mat_A, MatrixXf &mat_B){
	double tau_2 = 10;
	double tau_3 = 30;
	double tau_4 = 0.8;
	double tau_5 = 2;
	double tau_6 = 0.5;
	double beta_25 = 0.5; mat_A(0,3) = beta_25/tau_2;
	double beta_34 = 0.2; mat_A(1,2) = beta_34/tau_3;
	double beta_42 = 0.3; mat_A(2,0) = beta_42/tau_4;
	double beta_43 = 0.9; mat_A(2,1) = beta_43/tau_4;
	double beta_45 = 0.5; mat_A(2,3) = beta_45/tau_4;
	double beta_46 = 0.9; mat_A(2,4) = beta_46/tau_4;
	double beta_54 = 0.6; mat_A(3,2) = beta_54/tau_5;

	mat_A(0,0) = -1/tau_2;
	mat_A(1,1) = -1/tau_3;
	mat_A(2,2) = -1/tau_4;
	mat_A(3,3) = -1/tau_5;
	mat_A(4,4) = -1/tau_6;

	// u = [xi_4, xi_7, xi_8, xi_9, xi_10, xi_11]
	double gamma_29  = 2.5; mat_B(0,3) = gamma_29/tau_2;
	double gamma_311 = 0.4; mat_B(1,5) = gamma_311/tau_3;
	double gamma_57  = 1; mat_B(3,1) = gamma_57/tau_5;
	double gamma_510 = 0.6; mat_B(3,4) = gamma_510/tau_5;
	double gamma_64  = 1.5; mat_B(4,0) = gamma_64/tau_6;
	double gamma_68  = 2; mat_B(4,2) = gamma_68/tau_6;

}


int main(int argc, char const *argv[])
{
	// Tank model based on MPC Paper
	DynModel tank_model(5, 6);
	assign_Matrix_coeffs( tank_model.A, tank_model.B );

	// Print Matrices
	std::cout << "A:" << std::endl;
	tank_model.print_matrix( tank_model.A);
	std::cout << "size: (" << tank_model.A.rows() << "x" << tank_model.A.cols() << ")" << std::endl;

	std::cout << "B:" << std::endl;
	std::cout << "size: (" << tank_model.B.rows() << "x" << tank_model.B.cols() << ")" << std::endl;
	tank_model.print_matrix( tank_model.B);


	// Initialize x_k 
	VectorXf x_k;
	x_k.resize( tank_model.state_dim );
	for (size_t i = 0; i < x_k.rows(); i++){
		x_k(i) = 0.5;		
	}

	// Initialize zero input
	VectorXf u_k;
	u_k.resize( tank_model.control_dim );
	for (size_t i = 0; i < u_k.rows(); i++){
		u_k(i) = 0;		
	}

	// Calculate next step
	VectorXf x_k1;

	tank_model.set_xo_uo(x_k, u_k);	
	std::cout << "step 0" << std::endl;
	tank_model.print_state(); 

	int total_steps = 1000;
	for(size_t j = 0; j < total_steps; j++ ){
		VectorXf dx_k1 = tank_model.xdot_state_transition(tank_model.x, tank_model.u);
		x_k1 = dx_k1*tank_model.dt + x_k;
		tank_model.print_vector(u_k);
		tank_model.set_xo_uo(x_k1, u_k);

		std::cout << "step " << j << std::endl;	
		tank_model.print_state();
		x_k = x_k1;


	}



	return 0;
}