# Generate data for control problem.
import numpy as np
from dynamic_model import *

tank_model = DynModel(init_state_dim, init_control_dim)
assign_Matrix_coeffs(tank_model.A, tank_model.B)

Ax = tank_model.A * delta_t + np.identity(init_state_dim)
B1 = tank_model.B * delta_t
C = tank_model.C
x_0 = tank_model.eta_k

y_max = np.array([10000, 10000, 12000, 10000])
y_min = np.array([0,0,0,0])
u_min = np.array([500, 0, 0])
u_max = np.array([10000, 500, 500])

delta_u_min = np.array([-1000, -500, -500])
delta_u_max = np.array([1000, 500, 500])

U8 = np.array([5000, 6000, 7000, 8000, 9000, 10000])
U9 = np.array([100, 200, 300, 400, 500])
n_u8 = len(U8)
n_u9 = len(U9)
v_j = np.concatenate((U8,U9))


p = 3
m = 3
T = p
# Form and solve control problem.
from cvxpy import *
x = Variable(init_state_dim, T+1)
u = Variable(init_control_dim, T)
y = Variable(n_y, T+1)

num_bool = 3
# delta = Bool( num_bool, T ) #// Init SET [delta_10, delta_j=1, delta_j=2, ...delta_j=(n_u8+n_u9)]
delta = Bool( num_bool, T ) #// Init SET [delta_10, delta_j=1, delta_j=2, ...delta_j=(n_u8+n_u9)]

# delta = Variable( n_u8 + n_u9 + 1, T ) #// Init SET [delta_10, delta_j=1, delta_j=2, ...delta_j=(n_u8+n_u9)]
# bool_var = Bool(n_u8 + n_u9 +1)

Qy = np.zeros( (n_y, n_y) )
Qy[3][3] = 1.0

# qu = np.zeros( (dyn_model.n_u, dyn_model.n_u) )
# w_u9 = 0.003
# Qu[1][1] = w_u9

Qu = np.identity(init_control_dim)

Qdelta = np.identity(num_bool)

states = []
CC = np.array([1, 1, 1])
print CC

for t in range(T):
    # cost = sum_squares(x[:,t+1]) + quad_form(u[:,t], Qu) + quad_form(y[:, t+1], Qy) + quad_form( (delta[:, t] - np.ones(num_bool)), Qdelta)
    cost = quad_form(delta[:, t] , Qdelta)


    # cost =  quad_form(delta[:, t], Qdelta)

    constr = [x[:,t+1] == Ax*x[:,t] + B1*u[:,t] ,
              y[:,t+1] == C * x[:, t+1] ,
              delta[0, t] + delta[1, t] + delta[2, t] == 3.0 ]

              # norm(delta[0:n_u8,t]) == 1 ,
              # norm(delta[n_u8: n_u8 + n_u9, t]) == 1 ]

              # sum_squares(delta[0:n_u8,t]) == 1 ,
              # sum_squares(delta[n_u8: n_u8 + n_u9, t]) == 1 ]


    states.append( Problem(Minimize(cost), constr) )
    # states.append( Problem(Maximize(cost), constr) )
# sums problem objectives and concatenates constraints.
prob = sum(states)
# prob.solve( solver = ECOS_BB)
prob.solve()


print "u: "
print u.value

print "delta: "
print delta.value
# for i in range(n_u8 + n_u9 + 1):
#     print delta[i].value

# # Plot results.
# import matplotlib.pyplot as plt
# #matplotlib inline
# #config InlineBackend.figure_format = 'svg'

# f = plt.figure()

# # Plot (u_t)_1.
# ax = f.add_subplot(411)
# plt.plot(u[0,:].value.Ax.flatten())
# plt.ylabel(r"$(u_t)_1$", fontsize=16)
# plt.yticks(np.linspace(-1.0, 1.0, 3))
# plt.xticks([])

# # Plot (u_t)_2.
# plt.subplot(4,1,2)
# plt.plot(u[1,:].value.Ax.flatten())
# plt.ylabel(r"$(u_t)_2$", fontsize=16)
# plt.yticks(np.linspace(-1, 1, 3))
# plt.xticks([])

# # Plot (x_t)_1.
# plt.subplot(4,1,3)
# x1 = x[0,:].value.Ax.flatten()
# plt.plot(x1)
# plt.ylabel(r"$(x_t)_1$", fontsize=16)
# plt.yticks([-10, 0, 10])
# plt.ylim([-10, 10])
# plt.xticks([])

# # Plot (x_t)_2.
# plt.subplot(4,1,4)
# x2 = x[1,:].value.Ax.flatten()
# plt.plot(range(51), x2)
# plt.yticks([-25, 0, 25])
# plt.ylim([-25, 25])
# plt.ylabel(r"$(x_t)_2$", fontsize=16)
# plt.xlabel(r"$t$", fontsize=16)
# plt.tight_layout()
# plt.show()
