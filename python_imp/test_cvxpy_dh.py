import numpy as np
from cvxpy import *

# Create two scalar optimization variables.
dim = 3
x = Bool(dim)

weight_list = []
for i in range(dim):
    weight_list.append(3.0)
    
weight = np.array(weight_list)    
u = mul_elemwise(weight, x)

# u = []
# for i in range(dim):
#     u.append(3.0*x[i])
# C = [[2.0, 0.3, 0.1],
#      [0.3, 2.0, 0.1],
#      [0.1, 0.1, 5.0]];

# C_np = np.matrix(C)

# u = C_np * x

# Create two constraints.
constraints = [x[0] + x[1] + x[2] == 1,
               4*x[0] + 5*x[1] - 3 *x[2] >= 1]

# Form objective.
W = np.identity(dim)
print W
sqr_sum = quad_form(u, W)
obj = Minimize(sqr_sum)

# Form and solve problem.
prob = Problem(obj, constraints)
prob.solve()

# The optimal dual variable (Lagrange multiplier) for
# a constraint is stored in constraint.dual_value.
print "optimal (x + y == 1) dual variable", constraints[0].dual_value
print "optimal (x - y >= 1) dual variable", constraints[1].dual_value

for i in range(dim):
    print "%i th optimal value: %f" % (i, x[i].value)

