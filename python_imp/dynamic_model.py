import numpy as np

#---------------------------------------------------------------------------------------
# Initialize Bounds 
#---------------------------------------------------------------------------------------
init_state_dim = 5
init_control_dim = 6

T_s = 1 # 1 day
delta_t = 1

n_u = 3
n_y = 4

# eta = [eta_2, eta_3, eta_4, eta_5, eta_6]
# xi = [xi_4, xi_7, xi_8, xi_9, xi_10, xi_11]

def assign_Matrix_coeffs(A, B):
    tau_2 = 10.0
    tau_3 = 30.0
    tau_4 = 0.8
    tau_5 = 2.0
    tau_6 = 0.5
    beta_25 = 0.5 
    A[0][3] = beta_25/tau_2
    beta_34 = 0.2 
    A[1][2] = beta_34/tau_3
    beta_42 = 0.3 
    A[2][0] = beta_42/tau_4
    beta_43 = 0.9 
    A[2][1] = beta_43/tau_4
    beta_45 = 0.5 
    A[2][3] = beta_45/tau_4
    beta_46 = 0.9 
    A[2][4] = beta_46/tau_4
    beta_54 = 0.6 
    A[3][2] = beta_54/tau_5

    A[0][0] = -1/tau_2
    A[1][1] = -1/tau_3
    A[2][2] = -1/tau_4
    A[3][3] = -1/tau_5
    A[4][4] = -1/tau_6

    # xi = [xi_4, xi_7, xi_8, xi_9, xi_10, xi_11]
    gamma_29  = 2.5 
    B[0][3] = gamma_29/tau_2
    gamma_311 = 0.4 
    B[1][5] = gamma_311/tau_3
    gamma_57  = 1.0
    B[3][1] = gamma_57/tau_5
    gamma_510 = 0.6 
    B[3][4] = gamma_510/tau_5
    gamma_64  = 1.5 
    B[4][0] = gamma_64/tau_6
    gamma_68  = 2.0 
    B[4][2] = gamma_68/tau_6
#---------------------------------------------------------------------------------------
# End of initialization
#---------------------------------------------------------------------------------------

class DynModel:
    def __init__(self, state_dim, control_dim):
        self.A = np.zeros( (state_dim, state_dim) )
        self.B = np.zeros( (state_dim, control_dim))
        self.C = np.zeros( (n_y, state_dim) )
        self.C[2][2] = 1

        self.eta_k = np.zeros(state_dim)
        self.xi_k = np.zeros(control_dim)
        self.dt = delta_t
        self.state_dim = state_dim
        self.ctrl_dim = control_dim
        # Initialization values
        for i in range(state_dim):
            self.eta_k[i] = 0.5
        self.eta_k[2] = 0.0
            
    def compute_x_k1(self):
        eta_k = None
        iters = int(T_s / delta_t)
        for i in range(0, iters):
          # xdot = Ax + Bu            
          deta_k = (self.A.dot(self.eta_k ) + self.B.dot(self.xi_k ) )
          # x_k+1 = xdot*dt + x_k
          eta_k1 = deta_k*self.dt + self.eta_k
          # x_k, u_k = x_k+1, u_k+1
          self.set_eta_k_xi_k(eta_k1, self.xi_k)
        return eta_k1

    def compute_xi_input(self, state, delta):
        # TODO
        xi_input = np.zeros(self.control_dim)
        return xi_input

    def compute_u(self, delta):
        # TODO
        u = np.zeros(n_u)
        return u
        
        
    def compute_y_k1(self):
        # y = [y2, y3, y4, y5]
        return self.C.dot(self.eta_k)

    def set_eta_k_xi_k(self, eta_in, xi_in):
        self.eta_k = eta_in
        self.xi_k = xi_in
