import happiness_dynamics as robot
import mpc_happiness as mpc
import data_sender as udp_send
import numpy as np

m = 3
num_steps = 100

sim_model = robot.DynModel(robot.init_state_dim, robot.init_control_dim)
robot.assign_Matrix_coeffs(sim_model.A, sim_model.B)


data = np.zeros(sim_model.state_dim+1)
curr_state = sim_model.eta_k
ctrl_idx = 0
for i in range(num_steps):
    
    if (i%m == 0):
        [result_x, y, delta, u_opt, z] = mpc.run_mpc(curr_state)
        ctrl_idx = 0
        print u_opt.value
    
    u = u_opt.value[:, ctrl_idx]

    # print "input: "
    # print u

    sim_model.set_eta_k_xi_k(curr_state, u)
    curr_state = sim_model.compute_x_k1()

    # print "state: "
    # print curr_state
    
    data[0] = i * robot.delta_t;
    for j in range(sim_model.state_dim):
        data[j+1] = curr_state[j]
        
    udp_send.update(data)
    udp_send.send()

    ctrl_idx += 1
