from cvxpy import *
from happiness_dynamics import *
import numpy as np

model = DynModel(init_state_dim, init_control_dim)
assign_Matrix_coeffs(model.A, model.B)

print model.A
print model.B
print model.eta_k.shape

Ax = model.A * delta_t + np.identity(init_state_dim)
B1 = model.B * delta_t
C = model.C
x_0 = model.eta_k


### MPC paramters
num_bool = 3
p = 5 # controller horizon p
T = p
# x = [x_pos, battery, happiness, irritation]
# u = [move, charge, tell_jokes]
# y = [battery, happiness, irritation]


x = Variable(init_state_dim, T+1)
u = Variable(init_control_dim, T)
z = Variable(1, T)
y = Variable(n_y, T+1)
delta = Bool(num_bool, T)
#delta = [delta_battery, delta_charge, delta_joke]


# Constraint parameters
u_move_min = -12
u_move_max = 12

battery_max = 1.0
battery_min = 0
happiness_max = 1.0
x_pos_max = 10
irritation_min = 0

battery_level_no_movement = 0.2
battery_pos = 1
dist_tresh = 0.0
human_pos = 9


# Weights
w_battery = 1.0
w_happy = 1.0
w_irrit = 1.5

w_y = np.array([w_battery, w_happy, w_irrit])
Qy = np.matrix([ [w_battery, 0, 0],
                 [0, w_happy, 0],
                 [0, 0, w_irrit] ])
print Qy

y_ref = [battery_max, happiness_max, irritation_min]
def run_mpc(starting_x_state=model.eta_k):
  states = []
  for t in range(T):
      cost = quad_form((y[:, t+1] - y_ref), Qy)
      constr = [x[:,t+1] == Ax*x[:,t] + B1*u[:,t] ,
                y[:,t+1] == C * x[:, t+1],
                # ucharge = delta_charge
                # ujoke = delta_joke 
                u[1:,t] == delta[1:,t],
                # umove = delta_bat*z_move:
                u[0,t] >= delta[0,t]*u_move_min,
                u[0,t] <= delta[0,t]*u_move_max,
                # u[0,t] >= delta[0,t]*u_move_min,
                # u[0,t] <= delta[0,t]*u_move_max,

                #u[0,t] - z[0,t] <= (delta[0,t])*(u_move_max - u_move_min),
                #u[0,t] - z[0,t] >= (delta[0,t])*(u_move_min - u_move_max),

                # Battery level constraint:
                x[1,t] - battery_level_no_movement <= delta[0,t]*(battery_max - battery_min),
                x[1,t] - battery_level_no_movement >= (1 - delta[0,t])*(battery_min - battery_max),
                # Distance to battery constraint:
                (x[0,t] - battery_pos) - (dist_tresh) <= (1-delta[1,t])*x_pos_max,
                (x[0,t] - battery_pos) - (dist_tresh) >= delta[1,t]*(-x_pos_max), 
                # Distance to human constraint:
                (x[0,t] - human_pos) - (dist_tresh) <= (delta[2,t])*x_pos_max,
                (x[0,t] - human_pos) - (dist_tresh) >= (1-delta[2,t])*(-x_pos_max) ]
      states.append( Problem(Minimize(cost), constr) )
  prob = sum(states)
  prob.constraints += [x[:,0] == starting_x_state]
  prob.solve( solver = ECOS_BB)

  print "u:"
  print u.value
  print "delta:"
  print delta.value

  return x,y,delta,u,z

run_mpc()

print 'robot x pos'
print x.value[0,:]
print 'robot battery'
print x.value[1,:]
print 'robot happiness'
print x.value[2,:]
print 'human irritation'
print x.value[3,:]

print 'delta battery'
print delta.value[0,:]

print 'robot move effort'
print u.value[0,:]
