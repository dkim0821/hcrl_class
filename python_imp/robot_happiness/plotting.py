import socket
import numpy as np
import matplotlib.animation as animation
import matplotlib.pyplot as plt

UDP_IP = "127.0.0.1"
UDP_PORT = 5005

sock = socket.socket(socket.AF_INET, # Internet
                     socket.SOCK_DGRAM) # UDP
sock.bind((UDP_IP, UDP_PORT))

##=== State
fig_state = plt.figure()

num_state = 4
data_list = []

line_state = []
fig_ax = []

for i in range(0, num_state):
    align_state = '%d1%d' % (num_state, i+1)
    ## Position
    fig_ax.append( fig_state.add_subplot(align_state) )
    fig_ax[i].autoscale_view(True,True,True)
    fig_ax[i].grid()
    
    line, = fig_ax[i].plot([], [], 'r-', lw = 2)
    line_state.append(line)
    data_list.append([])
    

def init_state():
    for i in range (0, num_state):
        line_state[i].set_data([], [])
    return

time_list = []

def plot_data(i):
    global time_list, data_list, act_list
    # Link locations
    data, addr = sock.recvfrom(1024) # buffer size is 1024 bytes
    num_string = data.split(', ')
    
    sim_time = float(num_string[0])
    print "simulation time: %f" %(sim_time)
    
    # Initialize
    if sim_time < 0.05:
        time_list = []
        for k in range(0, num_state):
            data_list[k] = []
            line_state[k].set_data([], [])
                
    time_list.append(sim_time)

    for k in range(0, num_state):
        data_list[k].append(float(num_string[k + 1]))
        line_state[k].set_data(time_list, data_list[k])

        max_data = max(data_list[k]) + 0.01
        min_data =  min(data_list[k]) - 0.01
        fig_ax[k].set_ylim(min_data, max_data)
        fig_ax[k].set_xlim(min(time_list), max(time_list))

    return line_state


state = animation.FuncAnimation(fig_state, plot_data, np.arange(1, 800),
                               interval = 5, blit = False, init_func = init_state)

plt.show()        
    
    
