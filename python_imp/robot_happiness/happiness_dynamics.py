import numpy as np

#---------------------------------------------------------------------------------------
# Initialize Bounds 
#---------------------------------------------------------------------------------------
# x = [x_pos, battery, happiness, irritation]
# u = [move, charge, tell_jokes]
# y = [battery, happiness, irritation]
init_state_dim = 4
init_control_dim = 3

delta_t = 0.1

n_u = 3
n_y = 3

def assign_Matrix_coeffs(A, B):
    tau_battery = 20.0
    tau_happiness = 1.0
    tau_irritation = 0.25

    beta_battery = 0.5
    beta_irritation = 0.1

    gamma_move = 3.0
    gamma_charge = 0.35
    gamma_joke_happiness = 0.6
    gamma_joke_irritation = 1.0


    A[1][1] = -1.0/tau_battery
    A[2][1] = beta_battery/tau_happiness
    A[2][2] = -1.0/tau_happiness
    A[2][3] = -beta_irritation/tau_happiness
    A[3][3] = -1.0/tau_irritation

    B[0][0] = gamma_move
    B[1][1] = gamma_charge
    B[2][2] = gamma_joke_happiness
    B[3][2] = gamma_joke_irritation

#---------------------------------------------------------------------------------------
# End of initialization
#---------------------------------------------------------------------------------------

class DynModel:
    def __init__(self, state_dim, control_dim):
        self.A = np.zeros( (state_dim, state_dim) )
        self.B = np.zeros( (state_dim, control_dim))
        self.C = np.zeros( (n_y, state_dim) )
        self.C[0][1] = 1
        self.C[1][2] = 1
        self.C[2][3] = 1

        self.eta_k = np.zeros(state_dim)
        self.xi_k = np.zeros(control_dim)
        self.dt = delta_t
        self.state_dim = state_dim
        self.ctrl_dim = control_dim

        # Initialization values
        self.eta_k[0] = 5.0
        self.eta_k[1] = 0.4
        self.eta_k[2] = 0.0
        self.eta_k[3] = 0.0
            
    def compute_x_k1(self):
        # xdot = Ax + Bu            
        deta_k = (self.A.dot(self.eta_k ) + self.B.dot(self.xi_k ) )
        # x_k+1 = xdot*dt + x_k
        eta_k1 = deta_k*self.dt + self.eta_k
        # x_k, u_k = x_k+1, u_k+1
        return eta_k1

    def compute_xi_input(self, state, delta):
        # TODO
        xi_input = np.zeros(self.control_dim)
        return xi_input

    def compute_u(self, delta):
        # TODO
        u = np.zeros(n_u)
        return u
        
        
    def compute_y_k1(self):
        # y = [y2, y3, y4, y5]
        return self.C.dot(self.eta_k)

    def set_eta_k_xi_k(self, eta_in, xi_in):
        for i in range(self.state_dim):
            self.eta_k[i] = eta_in[i]

        for j in range(self.ctrl_dim):
            self.xi_k[j] = xi_in[j]
