from cvxpy import *
import numpy as np
import data_sender as udp_send

#---------------------------------------------------------------------------------------
# Initialize Bounds 
#---------------------------------------------------------------------------------------
init_state_dim = 5
init_control_dim = 6
n_u = 3
n_y = 4

T_s = 1 # 1 day
p = 7 # controller horizon p
m = 5 # controller horizon m

u_min = np.array([500, 0, 0])
u_max = np.array([10000, 500, 500])

delta_u_min = np.array([-1000, -500, -500])
delta_u_max = np.array([1000, 500, 500])

y_min = np.array([0,0,0,0])
y_max = np.array([10000, 10000, 12000, 10000])
delta_t = 0.001

U8 = np.array([5000, 6000, 7000, 8000, 9000, 10000])
U9 = np.array([100, 200, 300, 400, 500])
n_u8 = len(U8)
n_u9 = len(U9)



Qy = np.zeros( (n_y, n_y) )
Qy[3][3] = 1.0

Qu = np.zeros( (n_u, n_u) )
w_u9 = 0.003
Qu[1][1] = w_u9

# eta = [eta_2, eta_3, eta_4, eta_5, eta_6]
# xi = [xi_4, xi_7, xi_8, xi_9, xi_10, xi_11]

def assign_Matrix_coeffs(A, B):
    tau_2 = 10.0
    tau_3 = 30.0
    tau_4 = 0.8
    tau_5 = 2.0
    tau_6 = 0.5
    beta_25 = 0.5 
    A[0][3] = beta_25/tau_2
    beta_34 = 0.2 
    A[1][2] = beta_34/tau_3
    beta_42 = 0.3 
    A[2][0] = beta_42/tau_4
    beta_43 = 0.9 
    A[2][1] = beta_43/tau_4
    beta_45 = 0.5 
    A[2][3] = beta_45/tau_4
    beta_46 = 0.9 
    A[2][4] = beta_46/tau_4
    beta_54 = 0.6 
    A[3][2] = beta_54/tau_5

    A[0][0] = -1/tau_2
    A[1][1] = -1/tau_3
    A[2][2] = -1/tau_4
    A[3][3] = -1/tau_5
    A[4][4] = -1/tau_6

    # xi = [xi_4, xi_7, xi_8, xi_9, xi_10, xi_11]
    gamma_29  = 2.5 
    B[0][3] = gamma_29/tau_2
    gamma_311 = 0.4 
    B[1][5] = gamma_311/tau_3
    gamma_57  = 1.0
    B[3][1] = gamma_57/tau_5
    gamma_510 = 0.6 
    B[3][4] = gamma_510/tau_5
    gamma_64  = 1.5 
    B[4][0] = gamma_64/tau_6
    gamma_68  = 2.0 
    B[4][2] = gamma_68/tau_6
#---------------------------------------------------------------------------------------
# End of initialization
#---------------------------------------------------------------------------------------

class DynModel:
    def __init__(self, state_dim, control_dim):
        self.A = np.zeros( (state_dim, state_dim) )
        self.B = np.zeros( (state_dim, control_dim))
        self.C = np.zeros( (n_y, state_dim) )
        self.C[2][2] = 1

        self.eta_k = np.zeros(state_dim)
        self.xi_k = np.zeros(control_dim)
        self.dt = delta_t
        self.time = 0
        self.state_dim = state_dim

    def compute_x_k1(self):
        eta_k = None
        iters = int(T_s / delta_t)
        for i in range(0, iters):
          # xdot = Ax + Bu            
          deta_k = (self.A.dot(self.eta_k) + self.B.dot(self.xi_k))
          # x_k+1 = xdot*dt + x_k
          eta_k1 = deta_k*self.dt + self.eta_k
          # x_k, u_k = x_k+1, u_k+1
          self.set_eta_k_xi_k(eta_k1, self.xi_k)
          self.time = self.time + self.dt
          if (i% 30) == 1:
              self.data_sending()
        return eta_k1

    def compute_y_k1(self):
        # y = [y2, y3, y4, y5]
        return self.C.dot(self.eta_k)

    def set_eta_k_xi_k(self, eta_in, xi_in):
        self.eta_k = eta_in
        self.xi_k = xi_in

    def data_sending(self):
        x = []
        x.append(self.time)
        for i in range(0, self.state_dim):
            x.append(self.eta_k[i])
        udp_send.update(x)
        udp_send.send()


tank_model = DynModel(init_state_dim, init_control_dim)
assign_Matrix_coeffs(tank_model.A, tank_model.B)

print tank_model.A
print tank_model.B
print tank_model.eta_k.shape

print 'compute next day'
eta_in = np.zeros(init_state_dim)
xi_in = np.zeros(init_control_dim)

# Initialize eta
for i in range(init_state_dim):
    eta_in[i] = 0.5

print 'model prior'
print tank_model.eta_k
tank_model.set_eta_k_xi_k(eta_in, xi_in)
print 'model after'
print tank_model.eta_k

print 'compute next day'
print tank_model.compute_x_k1()


print tank_model.compute_y_k1()

def delta(k):
    return Bool( n_8 + n_9 + 1)

# j = 0 ... (n_u8 + n_9 + 1 - 1)
delta_k = Bool( (n_u8 + n_u9 + 1)*p ) #// Init SET [delta_10, delta_j=1, delta_j=2, ...delta_j=(n_u8+n_u9)]
# (delta_1, ... delta_n_u8, delta_n_u8+1 , ... delta_n_u9+n_u8) ... ,
# (delta_1, ... delta_n_u8, delta_n_u8+1 , ... delta_n_u9+n_u8)_p,
#  delta_10, ..., delta_10 *p

delta_k = np.ones( (n_u8 + n_u9 + 1)*p  )


v_j = np.concatenate((U8,U9))
v_j_actual = []
for i in range(p):
    for j in range(len(v_j)):
        v_j_actual.append(v_j[j])    
v_j = np.array(v_j_actual)

z_k = np.multiply(v_j, delta_k[0:len(v_j)] )


constraints = []
for i in range(p):
    # sum_of_delta_j = 1 from j=0 to (n_u8 - 1) // EQ constraint
   constraints.append( np.sum ( delta_k[i*(n_u8 + n_u9) : (i)*(n_u8+n_u9)+ n_u8 ] ) == 1  )
    # # sum_of_delta_j = 1 from j=n_u8 to (n_u8 + n_u9 -1) // EQ Constraint
   constraints.append( np.sum ( delta_k[i*(n_u8 + n_u9)+n_u8 : (i)*(n_u8+n_u9)+ (n_u8+n_u9) ] ) == 1  )

u8_actual_k = []
u9_actual_k = []
for i in range(p):
    # u_8(k) = sum of z_j(k) from j=0 to (n_u8 - 1) // SET 
    u8_actual_k .append( np.sum( z_k[i*(n_u8 + n_u9) : (i)*(n_u8+n_u9)+ n_u8 ] ) )
    # u_9(k) = sum of z_j(k) from j = n_u8 to (n_u8 + n_u9 -1) // SET
    u9_actual_k .append( np.sum( z_k[i*(n_u8 + n_u9)+n_u8 : (i)*(n_u8+n_u9) + (n_u8+n_u9)  ] ) )
u8_k = np.array( u8_actual_k  )
u9_k = np.array( u9_actual_k  )



#u_10(k) = z_10(k) // SET
#xi_10(k) = u_10(k) // SET

# y_kp1_actual = [ y_vec2 ,..., y_vecp ]
    
# for i in range(p-1):
#     (xi_4, xi_9, xi_10, xi_11, xi_7) = (0,0,0,0,0)
#     if i == 0:
#         xi_4 = u8_k[i] - 0
#         xi_10 = 0
#     else:
#         xi_4 = u8_k[i] - y_kp1_actual[i][2] # Element 3 of vector i

#     xi_9 = u9_k


#     tank_model.set_eta_k_xi_k( start_state, start_input )
#     tank_model.compute_x_k1()
#     tank_model.compute_y_k1()


#z_k = v_j*delta_k[0:(n_u8 + n_u9)*p]

# sum_of_delta_j = 1 from j=0 to (n_u8 - 1) // EQ constraint
# u_8(k) = sum of z_j(k) from j=0 to (n_u8 - 1) // SET 

# # sum_of_delta_j = 1 from j=n_u8 to (n_u8 + n_u9 -1) // EQ Constraint
# u_9(k) = sum of z_j(k) from j = n_u8 to (n_u8 + n_u9 -1) // SET

# y_4(k) = eta_4(k) // SET
# xi_4(k) = u_8(k) - y_4(k) // SET
# xi_9(k) = u_9(k)  // SET
# u_10(k) = z_10(k) // SET
# xi_10(k) = u_10(k) // SET
# xi_11(k) = u_11(k) = y_4(k) - u_8(k)
# xi_7(k) = 0
#
# y_4(k) - u_8(k-1) <= delta_10(k) * (y4_max - u8_min)
# y_4(k) - u_8(k-1) >= (1 - delta_10(k)) * (y4_max - u8_min)

# u_9(k-1) - z_10(k) <= (1 - delta_10(k)) * (u9_max - u10_min)
# u_9(k-1) - z_10(k) >= (1 - delta_10(k)) * (u9_min - u10_max)




# delta_j = Bool(20)
# print delta_j.value


#Z = Int(5, 7)
# u_k[0] = delta_j[0] * v_k[0]

# miniimize ||u_k - u_ref||^2
# subject to constraints:
# constraint : [u_10 = z_10]
#             delta_j + delta_j+1 ... = 1

