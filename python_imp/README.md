Download Anaconda
https://www.continuum.io/downloads#linux

#go to directory of downloaded file and install anaconda
$bash Anaconda2-4.1.1-Linux-x86_64.sh 


#Place the following in your ~/.bashrc:
export PATH="/home/username/anaconda2/bin:$PATH"

# source ~/.bashrc


----
Installing cvxpy
From: http://www.cvxpy.org/en/latest/install/index.html
----
# Then install cvxpy:
$conda install -c cvxgrp cvxpy

# Test installation
$conda install nose
$nosetests cvxpy

# tutorial on how use cvxpy:
http://www.cvxpy.org/en/latest/tutorial/advanced/

http://www.cvxpy.org/en/latest/examples/index.html
http://nbviewer.jupyter.org/github/cvxgrp/cvx_short_course/blob/master/intro/control.ipynb