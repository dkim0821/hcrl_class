from cvxpy import *
import data_sender as udp_send
import dynamic_model as dyn_model
import numpy as np

tank_model = dyn_model.DynModel(dyn_model.init_state_dim, dyn_model.init_control_dim)
dyn_model.assign_Matrix_coeffs(tank_model.A, tank_model.B)

print tank_model.A
print tank_model.B
print tank_model.eta_k.shape

num_days = 30;

def data_sending(day, state):
    x = []
    x.append(day)

    for i in state:
        x.append(i)

    udp_send.update(x)
    udp_send.send()


### MPC paramters
p = 5 # controller horizon p
m = 3 # controller horizon m

y_max = np.array([10000, 10000, 12000, 10000])
y_min = np.array([0,0,0,0])
u_min = np.array([500, 0, 0])
u_max = np.array([10000, 500, 500])

delta_u_min = np.array([-1000, -500, -500])
delta_u_max = np.array([1000, 500, 500])

U8 = np.array([5000, 6000, 7000, 8000, 9000, 10000])
U9 = np.array([100, 200, 300, 400, 500])
n_u8 = len(U8)
n_u9 = len(U9)

Qy = np.zeros( (dyn_model.n_y, dyn_model.n_y) )
Qy[3][3] = 1.0

Qu = np.zeros( (dyn_model.n_u, dyn_model.n_u) )
w_u9 = 0.003
Qu[1][1] = w_u9
### End MPC paramters

def run_mpc(model, u_pre):
    # j = 0 ... (n_u8 + n_9 + 1 - 1)
    delta_k = Bool( (n_u8 + n_u9 + 1)*p ) #// Init SET [delta_10, delta_j=1, delta_j=2, ...delta_j=(n_u8+n_u9)]
    # (delta_1, ... delta_n_u8, delta_n_u8+1 , ... delta_n_u9+n_u8) ... ,
    # (delta_1, ... delta_n_u8, delta_n_u8+1 , ... delta_n_u9+n_u8)_p,
    #####  delta_10, ..., delta_10 *p
    # delta_k = np.ones( (n_u8 + n_u9)* p  )
    v_j = np.concatenate((U8,U9))
    v_j_actual = []

    for i in range(p):
        for j in range(len(v_j)):
            v_j_actual.append(v_j[j])    

    v_j = np.array(v_j_actual)
    z_k = np.multiply(v_j, delta_k[0:len(v_j)] )

    constraints = []
    for i in range(p):
        # sum_of_delta_j = 1 from j=0 to (n_u8 - 1) // EQ constraint
        constraints.append( np.sum ( delta_k[i*(n_u8 + n_u9) : (i)*(n_u8+n_u9)+ n_u8 ] ) == 1  )
        # # sum_of_delta_j = 1 from j=n_u8 to (n_u8 + n_u9 -1) // EQ Constraint
        constraints.append( np.sum ( delta_k[i*(n_u8 + n_u9)+n_u8 : (i)*(n_u8+n_u9)+ (n_u8+n_u9) ] ) == 1  )

    
    u8_actual_k = [ u_pre[0] ]
    u9_actual_k = [ u_pre[1] ]
    u10_actual_k = [ u_pre[2] ]
    for i in range(p):
        # u_8(k) = sum of z_j(k) from j=0 to (n_u8 - 1) // SET 
        u8_actual_k .append( np.sum( z_k[i*(n_u8 + n_u9) : (i)*(n_u8+n_u9)+ n_u8 ] ) )
        # u_9(k) = sum of z_j(k) from j = n_u8 to (n_u8 + n_u9 -1) // SET
        u9_actual_k .append( np.sum( z_k[i*(n_u8 + n_u9)+n_u8 : (i)*(n_u8+n_u9) + (n_u8+n_u9)  ] ) )

    # u8_k = np.array( u8_actual_k  )
    # u9_k = np.array( u9_actual_k  )
    u8_k = u8_actual_k 
    u9_k = u9_actual_k 

    x_act_k = []

    
    input_list = []
    
    delta_10 = np.zeros(p)
    z10 = [0 for j in range(p) ]
    y_actual_k = []
    print "cccccc"
    for i in range(p):
        # xi = [xi_4, xi_7, xi_8, xi_9, xi_10, xi_11]
        model.compute_x_k1()
        x_act_k.append(model.eta_k)

        y_curr = model.compute_y_k1()
        y_actual_k.append(y_curr)
        y4_kp1 = y_curr[2]
        # xi_curr = np.zeros(model.ctrl_dim);
        xi_curr = [0, 0, 0, 0, 0, 0]
        xi_4 = u8_k[i] - y4_kp1
        xi_9 = u9_k[i]

        # granted pt
        u10 = 0
        # if (y4_kp1 >= u8_k[i]):
        if (True):
            delta_10[i] = 1
            u10 = u9_k[i]

        u10_actual_k.append(u10)
        z10[i] = u10
        xi_10 = u10

        # Goal attainment
        xi_11 = y4_kp1 - u8_k[i]
        xi_7 = 0.0
        xi_8 = 0.0

        input_list.append(np.array( [u8_k[i], u9_k[i], u10] ))

        xi_curr = [xi_4, xi_7, xi_8, xi_9, xi_10, xi_11]
        model.set_eta_k_xi_k(x_act_k[i], xi_curr)

        print i

    y_k = np.array ( y_actual_k )
    u10_k = np.array( u10_actual_k )
    print 1
    for i in range(p):
        y4_k = x_act_k[i][2]
        constraints.append( y4_k <= y_max[2] )
        constraints.append( y4_k >= y_min[2] )

        constraints.append( y4_k - u8_k[i] <= delta_10[i] * ( y_max[2] - u_min[2] ) )
        constraints.append( y4_k - u8_k[i] >= (1 - delta_10[i]) * (y_min[2] - u_max[2]) )

        constraints.append( u9_k[i] - z10[i] <= (1 - delta_10[i]) * (u_max[1] - u_min[2]) )
        constraints.append( u9_k[i] - z10[i] >= (1 - delta_10[i]) * (u_min[1] - u_max[2]) )

        constraints.append( z10[i] >= delta_10[i] * u_min[2] )
        constraints.append( z10[i] <= delta_10[i] * u_max[2] )
                            
    print 2
    for i in range(m-1):
        input_k = input_list[i]
        delta_input = input_list[i+1] - input_list[i]

        for j in range(len(input_k)):
            constraints.append(input_k[j] <= u_max[j])
            constraints.append(input_k[j] >= u_min[j])
            constraints.append(delta_input[j] <= delta_u_max[j])
            constraints.append(delta_input[j] >= delta_u_min[j])

    J = 0
    y_2_seq = []
    for k in range(p-2):
        i = k+2
        y_2_seq.append(y_actual_k[i][2])
        # J += y_actual_k[i][2] * y_actual_k[i][2]

    constraints = [delta_k[0] == delta_k[1]]

    J = square(y_k[0][2])
    for i in range(1,p):
        #print 'hello', i, len(y_k[i]), y_k[i][2]
        J += (square(y_k[i][2]))

    print 'hello'
    #J = 5;
    #constraints = [delta_k[0] == delta_k[1]]
    obj = Maximize(J)
    prob = Problem(obj, constraints)
    print "Start Optimization"
    prob.solve()
    print delta_k.value

    #u_10(k) = z_10(k) // SET
#xi_10(k) = u_10(k) // SETm 

# y_kp1_actual = [ y_vec2 ,..., y_vecp ]
    
# for i in range(p-1):
#     (xi_4, xi_9, xi_10, xi_11, xi_7) = (0,0,0,0,0)
#     if i == 0:
#         xi_4 = u8_k[i] - 0
#         xi_10 = 0
#     else:
#         xi_4 = u8_k[i] - y_kp1_actual[i][2] # Element 3 of vector i

#     xi_9 = u9_k


#     tank_model.set_eta_k_xi_k( start_state, start_input )
#     tank_model.compute_x_k1()
#     tank_model.compute_y_k1()


#z_k = v_j*delta_k[0:(n_u8 + n_u9)*p]

# sum_of_delta_j = 1 from j=0 to (n_u8 - 1) // EQ constraint
# u_8(k) = sum of z_j(k) from j=0 to (n_u8 - 1) // SET 

# # sum_of_delta_j = 1 from j=n_u8 to (n_u8 + n_u9 -1) // EQ Constraint
# u_9(k) = sum of z_j(k) from j = n_u8 to (n_u8 + n_u9 -1) // SET

# y_4(k) = eta_4(k) // SET
# xi_4(k) = u_8(k) - y_4(k) // SET
# xi_9(k) = u_9(k)  // SET
# u_10(k) = z_10(k) // SET
# xi_10(k) = u_10(k) // SET
# xi_11(k) = u_11(k) = y_4(k) - u_8(k)
# xi_7(k) = 0
#
# y_4(k) - u_8(k-1) <= delta_10(k) * (y4_max - u8_min)
# y_4(k) - u_8(k-1) >= (1 - delta_10(k)) * (y4_max - u8_min)

# u_9(k-1) - z_10(k) <= (1 - delta_10(k)) * (u9_max - u10_min)
# u_9(k-1) - z_10(k) >= (1 - delta_10(k)) * (u9_min - u10_max)




# delta_j = Bool(20)
# print delta_j.value


#Z = Int(5, 7)
# u_k[0] = delta_j[0] * v_k[0]

# miniimize ||u_k - u_ref||^2
# subject to constraints:
# constraint : [u_10 = z_10]
#             delta_j + delta_j+1 ... = 1











ctrl_idx = 0
input_list = []
tot_eta_k = []

# Main loop
curr_state = tank_model.eta_k
u_pre = np.array([0, 0, 0])
for i in range(num_days):
    # run mpc
    if (i % m) == 0:
        delta_list = run_mpc(tank_model, u_pre)
        ctrl_idx = 0

    xi_input = tank_model.compute_xi_input(curr_state, delta_list[ctrl_idx] )
    tank_model.set_eta_k_xi_k(curr_state, xi_input)
    tank_model.compute_x_k1()
    curr_state = tank_model.eta_k

    u_pre = tank_model.compute_u(delta_list[ctrl_idx])
    ctrl_idx += 1
            
    data_sending(i, curr_state)
